import * as urlJoin from 'url-join'

export function buildUrl (baseUrl: string, ...path: string[]) {
  if (arguments.length > 0) {
    const args = [baseUrl].concat(path)
    return urlJoin(...args)
  }
  return baseUrl
}

export function toQueryParams (params: { [key: string]: any }) {
  return Object.getOwnPropertyNames(params).map((key) => {
    return `${key}=${params[key]}`
  }).join('&')
}

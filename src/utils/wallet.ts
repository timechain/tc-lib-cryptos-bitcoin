import { ECPair } from 'bitcoinjs-lib'

import { CRYPTO } from './constants'
import { BitcoinWallet } from '../models/wallet'

export function getPublicKey (keypair: ECPair): string {
  return keypair.getPublicKeyBuffer().toString('hex')
}

export function walletFromWif (wif: string): BitcoinWallet {
  const keypair = ECPair.fromWIF(wif)
  return {
    crypto: CRYPTO,
    address: keypair.getAddress(),
    publicKey: getPublicKey(keypair),
    privateKey: wif
  }
}

export function isSameAddress (address1: string, address2: string): boolean {
  return (!!address1 && !!address2 && address1.toLowerCase() === address2.toLowerCase())
}

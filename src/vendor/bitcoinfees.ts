import axios from 'axios'
import { BigNumber } from 'bignumber.js'

import { CRYPTO } from '../utils/constants'
import { buildUrl, toQueryParams } from '../utils/url'
import { Transaction } from 'tc-lib-cryptos/dist/models'
import { TransactionFeesPolicy } from 'tc-lib-cryptos/dist/modules/wallet'

const BASE_URL = 'https://bitcoinfees.earn.com/api/v1'

export interface RecommandedFees {
  fastestFee: number
  halfHourFee: number
  hourFee: number
}

/**
 * Returns the recommended Satoshi per bytes fees
 * @param policy The fees policy
 */
export async function getRecommendedFees (policy: TransactionFeesPolicy): Promise<BigNumber> {
  const response = await axios.get<RecommandedFees>(buildUrl(BASE_URL, 'fees', 'recommended'))

  switch (policy) {
    case 'minimum':
      return new BigNumber(response.data.hourFee)
  }
}

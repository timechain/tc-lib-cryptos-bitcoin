import axios, { AxiosError } from 'axios'
import { BigNumber } from 'bignumber.js'
import * as FormDataFallback from 'form-data'

import { CRYPTO } from '../utils/constants'
import { isSameAddress } from '../utils/wallet'
import { buildUrl, toQueryParams } from '../utils/url'
import { Transaction } from 'tc-lib-cryptos/dist/models'

const BASE_URL = 'https://blockchain.info'
const CORS_FLAG = 'cors=true'

export interface TxOutput {
  spent: boolean
  tx_index: number
  type: number
  addr: string
  value: number
  n: number
  script: string
}

export interface Tx {
  ver: string
  inputs: {
    sequence: string
    witness: string
    prev_out: TxOutput,
    script: string
  }[]
  weight: number
  block_height: number
  relayed_by: string,
  out: TxOutput[]
  lock_time: number
  result: number
  size: number
  time: number
  tx_index: number
  vin_sz: number
  hash: string
  vout_sz: number
}

export interface BalanceResponse {
  [address: string]: {
    final_balance: number
    n_tx: number
    total_received: number
  }
}

export interface AddressResponse {
  hash160: string
  address: string
  n_tx: number
  total_received: number
  total_sent: number
  final_balance: number
  txs: Tx[]
}

export interface UTXO {
  tx_hash: string
  tx_hash_big_endian: string
  tx_index: number
  tx_output_n: number
  script: string
  value: number
  value_hex: string
  confirmations: number
}

export interface UXTOResponse {
  notice: string
  unspent_outputs: UTXO[]
}

function url (action: string, queryParams: { [key: string]: any }) {
  return buildUrl(BASE_URL, 'en', action + '?' + CORS_FLAG + '&' + toQueryParams(queryParams))
}

export async function getBalance (address: string): Promise<BigNumber> {
  const { data } = await axios.get<BalanceResponse>(url('balance', { active: address }))
  return new BigNumber(data[address].final_balance)
}

export async function getRawTransactions (address: string): Promise<Tx[]> {
  const { data } = await axios.get<AddressResponse>(buildUrl(BASE_URL, 'en', 'rawaddr', address))
  return data.txs
}

export async function getUnspentTransactions (address: string): Promise<UTXO[]> {
  const { data } = await axios.get<UXTOResponse>(url('unspent', {
    active: address
  }))
  return data.unspent_outputs
}

export async function getTransactions (address: string): Promise<Transaction[]> {
  const transactions: Transaction[] = []
  for (const rawTransaction of await getRawTransactions(address)) {
    transactions.push(...txToTransactions(rawTransaction, address, true))
  }
  return transactions
}

export async function sendTransaction (rawTransactionHex: string): Promise<void> {
  const url = buildUrl(BASE_URL, `pushtx?${CORS_FLAG}`)

  if (typeof FormData !== 'undefined') {
    // Web
    return new Promise<void>((res, rej) => {
      const body = new FormData()
      body.append('tx', rawTransactionHex)
      const request = new XMLHttpRequest()
      request.open('POST', url)
      request.onload = () => {
        res()
      }
      request.onerror = () => {
        rej(request.statusText)
      }
      request.send(body)
    })
  } else {
    // Node
    const body = new FormDataFallback()
    body.append('tx', rawTransactionHex)
    const { data } = await axios.post<any>(url, body, {
      headers : body.getHeaders()
    })
  }
}

/**
 * Splits a raw transaction to a list of transactions for every output
 * @param tx The raw transaction
 * @param address The address related to the transaction, to know wich input / output to choose.
 * @param ignoreCashbackOutputs Ignore outputs with same address as input, probably used for cashback
 */
function txToTransactions (tx: Tx, address: string, ignoreCashbackOutputs: boolean): Transaction[] {
  const transactions: Transaction[] = []
  const addressRelation = addressRelationToTransaction(tx, address)

  for (const out of tx.out) {
    // If the related address is a sender, set is as the sender, otherwise set first input as we can only specify one sender
    const sender = (addressRelation === 'sender') ? address : tx.inputs[0].prev_out.addr

    if (ignoreCashbackOutputs) {
      if (addressRelation === 'sender' && isSameAddress(address, out.addr)) continue
    }

    // Ignore related input - output combinations
    if (!isSameAddress(sender, address) && !isSameAddress(out.addr, address)) continue

    transactions.push({
      hash: tx.hash,
      date: new Date(tx.time * 1000),
      fromAddress: sender,
      toAddress: out.addr,
      amount: new BigNumber(out.value),
      crypto: CRYPTO
    })
  }

  return transactions
}

/**
 * Checks wether an address is rather a sender or a recipient or is unrelated to a transaction
 * by checking if the total amout as input is bigger than the total amount as output.
 * If the amount is the same, consider that the address is a sender.
 * @param tx The raw transaction
 * @param address The related address
 */
function addressRelationToTransaction (tx: Tx, address: string): 'sender' | 'recipient' | 'unrelated' {
  let amountAsSender: number = 0
  let amountAsRecipient: number = 0

  for (const input of tx.inputs) {
    if (isSameAddress(input.prev_out.addr, address)) amountAsSender += input.prev_out.value
  }

  for (const output of tx.out) {
    if (isSameAddress(output.addr, address)) amountAsRecipient += output.value
  }

  if (amountAsSender === 0 && amountAsRecipient === 0) return 'unrelated'
  if (amountAsSender > amountAsRecipient) return 'sender'
  if (amountAsRecipient > amountAsSender) return 'recipient'
  return 'sender'
}

import { BigNumber } from 'bignumber.js'
import * as coinselect from 'coinselect'
import * as addressValidator from 'cryptaddress-validator'
import { ECPair, TransactionBuilder, address as addressUtils } from 'bitcoinjs-lib'
import { WalletAdapter, SendTransactionOptions } from 'tc-lib-cryptos/dist/modules/wallet'

import { CRYPTO } from './utils/constants'
import { BitcoinWallet } from './models/wallet'
import { Transaction } from 'tc-lib-cryptos/dist/models'
import { getRecommendedFees } from './vendor/bitcoinfees'
import { getPublicKey, walletFromWif, isSameAddress } from './utils/wallet'
import { getBalance, getTransactions, getRawTransactions, Tx, sendTransaction, TxOutput, getUnspentTransactions, UTXO } from './vendor/blochain-info'

export class BitcoinAdapterClass implements WalletAdapter<BitcoinWallet> {
  crypto = CRYPTO

  name = 'Bitcoin'

  decimals = 8

  async generateRandomWallet (): Promise<{
    wallet: BitcoinWallet,
    seed: string
  }> {
    const keypair = ECPair.makeRandom()
    const wif = keypair.toWIF()
    return {
      wallet: walletFromWif(wif),
      seed: wif
    }
  }

  async getBalance (wallet: Partial<BitcoinWallet>): Promise<BigNumber> {
    return getBalance(wallet.address!)
  }

  async sendTransaction (options: SendTransactionOptions<BitcoinWallet>): Promise<Transaction> {
    const feesPerByte = await getRecommendedFees(options.fees)
    const { fees, transactionBuilder } = await this.buildTransaction({
      wallet: options.wallet,
      recipientAddress: options.recipientAddress,
      amount: options.amount,
      feeRate: feesPerByte
    })

    const transaction = transactionBuilder.build()

    await sendTransaction(transaction.toHex())

    return {
      hash: transaction.getId(),
      date: new Date(),
      fromAddress: options.wallet.address,
      toAddress: options.recipientAddress,
      amount: options.amount,
      crypto: CRYPTO
    }
  }

  async getTransactionFees (params: SendTransactionOptions<BitcoinWallet>): Promise<BigNumber> {
    const feesPerByte = await getRecommendedFees(params.fees)
    const { fees } = await this.buildTransaction({
      wallet: params.wallet,
      recipientAddress: params.recipientAddress,
      amount: params.amount,
      feeRate: feesPerByte
    })
    return new BigNumber(fees)
  }

  async getTransactions (wallet: Partial<BitcoinWallet>): Promise<Transaction[]> {
    if (!wallet.address) throw new Error('The provided wallet must have an address')
    return getTransactions(wallet.address)
  }

  validateAddress (address: string, options: object | null): boolean {
    return addressValidator(this.crypto.toLowerCase()).test(address)
  }

  async importWalletWithSeed (seed: string): Promise<BitcoinWallet> {
    return walletFromWif(seed)
  }

  async importWallet (info: {
    publicKey?: string;
    privateKey?: string;
    address?: string;
  }): Promise<BitcoinWallet> {
    if (!info.privateKey) throw new Error('Private key (seed) not provided.')
    // Consider the seed as the private key
    return this.importWalletWithSeed(info.privateKey)
  }

  /**
   * Returns the list of unspent transactions outputs of an address
   */
  async utxo (address: string): Promise<UTXO[]> {
    return getUnspentTransactions(address)
  }

  /**
   * Builds a ready-to-send transaction and calculate the total fees
   */
  private async buildTransaction (options: {
    wallet: BitcoinWallet
    recipientAddress: string
    amount: BigNumber
    /**
     * Fee rate in satoshi per bytes
     */
    feeRate: BigNumber
  }): Promise<{
    transactionBuilder: TransactionBuilder,
    fees: number
  }> {
    /*
     * Fetch UTXOs and convert them for coinsupport library
     */
    const utxos: {
      txId: string,
      vout: number,
      value: number
    }[] = []
    for (const utxo of await this.utxo(options.wallet.address)) {
      utxos.push({
        txId: utxo.tx_hash_big_endian,
        vout: utxo.tx_output_n,
        value: utxo.value
      })
    }

    const targets = [{
      address: options.recipientAddress,
      value: options.amount.toNumber()
    }]

    // Will calculate inputs and outputs required for the transactions + total fees
    let { inputs, outputs, fee } = coinselect(utxos, targets, options.feeRate.toNumber())

    // Error handling
    if (!inputs || !outputs) {
      throw new Error('Could not build transaction. Does the wallet have enough crypto?')
    }

    // Create a transaction with the result inputs and outputs
    const transactionBuilder = new TransactionBuilder()
    for (const input of inputs) {
      transactionBuilder.addInput(input.txId, input.vout)
    }
    for (const output of outputs) {
      // No address in output means output to give change back.
      if (!output.address) {
        output.address = options.wallet.address
      }

      transactionBuilder.addOutput(output.address, output.value)
    }

    // Sign transaction
    for (let i = 0; i < transactionBuilder.inputs.length; i++) {
      transactionBuilder.sign(i, ECPair.fromWIF(options.wallet.privateKey))
    }

    return {
      transactionBuilder: transactionBuilder,
      fees: fee
    }
  }
}

export const BitcoinAdapter = new BitcoinAdapterClass()

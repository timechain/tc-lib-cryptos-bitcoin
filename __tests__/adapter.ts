import { BigNumber } from 'bignumber.js'

import { BitcoinAdapter } from '../src'
import { CRYPTO } from '../src/utils/constants'
import { Wallet } from 'tc-lib-cryptos/dist/models'
import { BitcoinWallet } from '../src/models/wallet'

const TEST_ADDRESS = '3D2oetdNuZUqQHPJmcMDDHYoqkyNVsFk9r'

test('Get generic info', async () => {
  expect(BitcoinAdapter.name).toBe('Bitcoin')
  expect(BitcoinAdapter.crypto).toBe('BTC')
  expect(BitcoinAdapter.decimals).toBe(8)
})

test('Generate random wallet', async () => {
  const { wallet } = await BitcoinAdapter.generateRandomWallet()
  expect(BitcoinAdapter.validateAddress(wallet.address, null)).toEqual(true)
})

test('Import wallet', async () => {
  const wallet = await BitcoinAdapter.importWalletWithSeed('Kyr972iuBFh4yBzAziRDkh1V9LbvfdWePTm7Bj2sZzWra3bZDHHc')
  expect(BitcoinAdapter.validateAddress(wallet.address, null)).toEqual(true)
  expect(wallet.address).toEqual('14xUzmMWkfLH8GWJBxh8Uwnai7YRZEtPxu')
})

test('Validate address', () => {
  expect(BitcoinAdapter.validateAddress(TEST_ADDRESS, null)).toEqual(true)
  expect(BitcoinAdapter.validateAddress('abc', null)).toEqual(false)
  expect(BitcoinAdapter.validateAddress('bitcoincash:qpm2qsznhks23z7629mms6s4cwef74vcwvy22gdx6a', null)).toEqual(false)
})

test('Get balance', async () => {
  const balance = await BitcoinAdapter.getBalance({
    address: TEST_ADDRESS
  })
  expect(balance).toBeDefined()
})

test('Get transactions', async () => {
  const transactions = await BitcoinAdapter.getTransactions({
    address: TEST_ADDRESS
  })
  expect(transactions.length).toBeGreaterThan(0)
})

test('Estimate transactions fees', async () => {
  const wallet = (await BitcoinAdapter.generateRandomWallet()).wallet
  wallet.address = TEST_ADDRESS

  const fees = await BitcoinAdapter.getTransactionFees({
    wallet: wallet,
    recipientAddress: TEST_ADDRESS,
    amount: new BigNumber(12345),
    fees: 'minimum'
  })
  expect(fees.toNumber()).toBeGreaterThan(0)
})

# Bitcoin adapter library

Bitcoin adapter for [Timechain Lib Cryptos](https://bitbucket.org/timechain/tc-lib-cryptos).

## Unit tests

In a terminal, run:

```sh
npm run test
```

To be able to perform transactions, you must set some environment variables.

```bash
# The WIF of the wallet to use to send transactions
export WALLET_WIF="Kx2StqfkxHaEFFfXNTpVkESxKr3aTbiVu1jSRCYP9KVEsZm14R9g"

# The recipient address to send transactions
export RECIPIENT_ADDRESS="1MPsDH3sUD6xi5YWTbob5WARSKoFreKqbJ"

# Set no if you don't want to perform transactions in the tests
export PERFORM_TRANSACTIONS=true

```
104.192.143.3

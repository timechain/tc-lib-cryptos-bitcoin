"use strict";
exports.__esModule = true;
var bitcoinjs_lib_1 = require("bitcoinjs-lib");
var constants_1 = require("./constants");
function getPublicKey(keypair) {
    return keypair.getPublicKeyBuffer().toString('hex');
}
exports.getPublicKey = getPublicKey;
function walletFromWif(wif) {
    var keypair = bitcoinjs_lib_1.ECPair.fromWIF(wif);
    return {
        crypto: constants_1.CRYPTO,
        address: keypair.getAddress(),
        publicKey: getPublicKey(keypair),
        privateKey: wif
    };
}
exports.walletFromWif = walletFromWif;
function isSameAddress(address1, address2) {
    return (!!address1 && !!address2 && address1.toLowerCase() === address2.toLowerCase());
}
exports.isSameAddress = isSameAddress;

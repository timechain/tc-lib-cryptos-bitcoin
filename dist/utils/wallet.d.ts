import { ECPair } from 'bitcoinjs-lib';
import { BitcoinWallet } from '../models/wallet';
export declare function getPublicKey(keypair: ECPair): string;
export declare function walletFromWif(wif: string): BitcoinWallet;
export declare function isSameAddress(address1: string, address2: string): boolean;

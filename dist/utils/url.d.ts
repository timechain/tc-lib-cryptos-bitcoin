export declare function buildUrl(baseUrl: string, ...path: string[]): string;
export declare function toQueryParams(params: {
    [key: string]: any;
}): string;

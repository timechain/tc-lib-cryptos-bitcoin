import { BigNumber } from 'bignumber.js';
import { WalletAdapter, SendTransactionOptions } from 'tc-lib-cryptos/dist/modules/wallet';
import { BitcoinWallet } from './models/wallet';
import { Transaction } from 'tc-lib-cryptos/dist/models';
import { UTXO } from './vendor/blochain-info';
export declare class BitcoinAdapterClass implements WalletAdapter<BitcoinWallet> {
    crypto: string;
    name: string;
    decimals: number;
    generateRandomWallet(): Promise<{
        wallet: BitcoinWallet;
        seed: string;
    }>;
    getBalance(wallet: Partial<BitcoinWallet>): Promise<BigNumber>;
    sendTransaction(options: SendTransactionOptions<BitcoinWallet>): Promise<Transaction>;
    getTransactionFees(params: SendTransactionOptions<BitcoinWallet>): Promise<BigNumber>;
    getTransactions(wallet: Partial<BitcoinWallet>): Promise<Transaction[]>;
    validateAddress(address: string, options: object | null): boolean;
    importWalletWithSeed(seed: string): Promise<BitcoinWallet>;
    importWallet(info: {
        publicKey?: string;
        privateKey?: string;
        address?: string;
    }): Promise<BitcoinWallet>;
    /**
     * Returns the list of unspent transactions outputs of an address
     */
    utxo(address: string): Promise<UTXO[]>;
    /**
     * Builds a ready-to-send transaction and calculate the total fees
     */
    private buildTransaction(options);
}
export declare const BitcoinAdapter: BitcoinAdapterClass;

"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var bignumber_js_1 = require("bignumber.js");
var coinselect = require("coinselect");
var addressValidator = require("cryptaddress-validator");
var bitcoinjs_lib_1 = require("bitcoinjs-lib");
var constants_1 = require("./utils/constants");
var bitcoinfees_1 = require("./vendor/bitcoinfees");
var wallet_1 = require("./utils/wallet");
var blochain_info_1 = require("./vendor/blochain-info");
var BitcoinAdapterClass = /** @class */ (function () {
    function BitcoinAdapterClass() {
        this.crypto = constants_1.CRYPTO;
        this.name = 'Bitcoin';
        this.decimals = 8;
    }
    BitcoinAdapterClass.prototype.generateRandomWallet = function () {
        return __awaiter(this, void 0, void 0, function () {
            var keypair, wif;
            return __generator(this, function (_a) {
                keypair = bitcoinjs_lib_1.ECPair.makeRandom();
                wif = keypair.toWIF();
                return [2 /*return*/, {
                        wallet: wallet_1.walletFromWif(wif),
                        seed: wif
                    }];
            });
        });
    };
    BitcoinAdapterClass.prototype.getBalance = function (wallet) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, blochain_info_1.getBalance(wallet.address)];
            });
        });
    };
    BitcoinAdapterClass.prototype.sendTransaction = function (options) {
        return __awaiter(this, void 0, void 0, function () {
            var feesPerByte, _a, fees, transactionBuilder, transaction;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, bitcoinfees_1.getRecommendedFees(options.fees)];
                    case 1:
                        feesPerByte = _b.sent();
                        return [4 /*yield*/, this.buildTransaction({
                                wallet: options.wallet,
                                recipientAddress: options.recipientAddress,
                                amount: options.amount,
                                feeRate: feesPerByte
                            })];
                    case 2:
                        _a = _b.sent(), fees = _a.fees, transactionBuilder = _a.transactionBuilder;
                        transaction = transactionBuilder.build();
                        return [4 /*yield*/, blochain_info_1.sendTransaction(transaction.toHex())];
                    case 3:
                        _b.sent();
                        return [2 /*return*/, {
                                hash: transaction.getId(),
                                date: new Date(),
                                fromAddress: options.wallet.address,
                                toAddress: options.recipientAddress,
                                amount: options.amount,
                                crypto: constants_1.CRYPTO
                            }];
                }
            });
        });
    };
    BitcoinAdapterClass.prototype.getTransactionFees = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var feesPerByte, fees;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, bitcoinfees_1.getRecommendedFees(params.fees)];
                    case 1:
                        feesPerByte = _a.sent();
                        return [4 /*yield*/, this.buildTransaction({
                                wallet: params.wallet,
                                recipientAddress: params.recipientAddress,
                                amount: params.amount,
                                feeRate: feesPerByte
                            })];
                    case 2:
                        fees = (_a.sent()).fees;
                        return [2 /*return*/, new bignumber_js_1.BigNumber(fees)];
                }
            });
        });
    };
    BitcoinAdapterClass.prototype.getTransactions = function (wallet) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                if (!wallet.address)
                    throw new Error('The provided wallet must have an address');
                return [2 /*return*/, blochain_info_1.getTransactions(wallet.address)];
            });
        });
    };
    BitcoinAdapterClass.prototype.validateAddress = function (address, options) {
        return addressValidator(this.crypto.toLowerCase()).test(address);
    };
    BitcoinAdapterClass.prototype.importWalletWithSeed = function (seed) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, wallet_1.walletFromWif(seed)];
            });
        });
    };
    BitcoinAdapterClass.prototype.importWallet = function (info) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                if (!info.privateKey)
                    throw new Error('Private key (seed) not provided.');
                // Consider the seed as the private key
                return [2 /*return*/, this.importWalletWithSeed(info.privateKey)];
            });
        });
    };
    /**
     * Returns the list of unspent transactions outputs of an address
     */
    BitcoinAdapterClass.prototype.utxo = function (address) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, blochain_info_1.getUnspentTransactions(address)];
            });
        });
    };
    /**
     * Builds a ready-to-send transaction and calculate the total fees
     */
    BitcoinAdapterClass.prototype.buildTransaction = function (options) {
        return __awaiter(this, void 0, void 0, function () {
            var utxos, _i, _a, utxo, targets, _b, inputs, outputs, fee, transactionBuilder, _c, inputs_1, input, _d, outputs_1, output, i;
            return __generator(this, function (_e) {
                switch (_e.label) {
                    case 0:
                        utxos = [];
                        _i = 0;
                        return [4 /*yield*/, this.utxo(options.wallet.address)];
                    case 1:
                        _a = _e.sent();
                        _e.label = 2;
                    case 2:
                        if (!(_i < _a.length)) return [3 /*break*/, 4];
                        utxo = _a[_i];
                        utxos.push({
                            txId: utxo.tx_hash_big_endian,
                            vout: utxo.tx_output_n,
                            value: utxo.value
                        });
                        _e.label = 3;
                    case 3:
                        _i++;
                        return [3 /*break*/, 2];
                    case 4:
                        targets = [{
                                address: options.recipientAddress,
                                value: options.amount.toNumber()
                            }];
                        _b = coinselect(utxos, targets, options.feeRate.toNumber()), inputs = _b.inputs, outputs = _b.outputs, fee = _b.fee;
                        // Error handling
                        if (!inputs || !outputs) {
                            throw new Error('Could not build transaction. Does the wallet have enough crypto?');
                        }
                        transactionBuilder = new bitcoinjs_lib_1.TransactionBuilder();
                        for (_c = 0, inputs_1 = inputs; _c < inputs_1.length; _c++) {
                            input = inputs_1[_c];
                            transactionBuilder.addInput(input.txId, input.vout);
                        }
                        for (_d = 0, outputs_1 = outputs; _d < outputs_1.length; _d++) {
                            output = outputs_1[_d];
                            // No address in output means output to give change back.
                            if (!output.address) {
                                output.address = options.wallet.address;
                            }
                            transactionBuilder.addOutput(output.address, output.value);
                        }
                        // Sign transaction
                        for (i = 0; i < transactionBuilder.inputs.length; i++) {
                            transactionBuilder.sign(i, bitcoinjs_lib_1.ECPair.fromWIF(options.wallet.privateKey));
                        }
                        return [2 /*return*/, {
                                transactionBuilder: transactionBuilder,
                                fees: fee
                            }];
                }
            });
        });
    };
    return BitcoinAdapterClass;
}());
exports.BitcoinAdapterClass = BitcoinAdapterClass;
exports.BitcoinAdapter = new BitcoinAdapterClass();

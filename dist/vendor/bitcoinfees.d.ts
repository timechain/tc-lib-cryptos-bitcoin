import { BigNumber } from 'bignumber.js';
import { TransactionFeesPolicy } from 'tc-lib-cryptos/dist/modules/wallet';
export interface RecommandedFees {
    fastestFee: number;
    halfHourFee: number;
    hourFee: number;
}
/**
 * Returns the recommended Satoshi per bytes fees
 * @param policy The fees policy
 */
export declare function getRecommendedFees(policy: TransactionFeesPolicy): Promise<BigNumber>;

"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var axios_1 = require("axios");
var bignumber_js_1 = require("bignumber.js");
var FormDataFallback = require("form-data");
var constants_1 = require("../utils/constants");
var wallet_1 = require("../utils/wallet");
var url_1 = require("../utils/url");
var BASE_URL = 'https://blockchain.info';
var CORS_FLAG = 'cors=true';
function url(action, queryParams) {
    return url_1.buildUrl(BASE_URL, 'en', action + '?' + CORS_FLAG + '&' + url_1.toQueryParams(queryParams));
}
function getBalance(address) {
    return __awaiter(this, void 0, void 0, function () {
        var data;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, axios_1["default"].get(url('balance', { active: address }))];
                case 1:
                    data = (_a.sent()).data;
                    return [2 /*return*/, new bignumber_js_1.BigNumber(data[address].final_balance)];
            }
        });
    });
}
exports.getBalance = getBalance;
function getRawTransactions(address) {
    return __awaiter(this, void 0, void 0, function () {
        var data;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, axios_1["default"].get(url_1.buildUrl(BASE_URL, 'en', 'rawaddr', address))];
                case 1:
                    data = (_a.sent()).data;
                    return [2 /*return*/, data.txs];
            }
        });
    });
}
exports.getRawTransactions = getRawTransactions;
function getUnspentTransactions(address) {
    return __awaiter(this, void 0, void 0, function () {
        var data;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, axios_1["default"].get(url('unspent', {
                        active: address
                    }))];
                case 1:
                    data = (_a.sent()).data;
                    return [2 /*return*/, data.unspent_outputs];
            }
        });
    });
}
exports.getUnspentTransactions = getUnspentTransactions;
function getTransactions(address) {
    return __awaiter(this, void 0, void 0, function () {
        var transactions, _i, _a, rawTransaction;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    transactions = [];
                    _i = 0;
                    return [4 /*yield*/, getRawTransactions(address)];
                case 1:
                    _a = _b.sent();
                    _b.label = 2;
                case 2:
                    if (!(_i < _a.length)) return [3 /*break*/, 4];
                    rawTransaction = _a[_i];
                    transactions.push.apply(transactions, txToTransactions(rawTransaction, address, true));
                    _b.label = 3;
                case 3:
                    _i++;
                    return [3 /*break*/, 2];
                case 4: return [2 /*return*/, transactions];
            }
        });
    });
}
exports.getTransactions = getTransactions;
function sendTransaction(rawTransactionHex) {
    return __awaiter(this, void 0, void 0, function () {
        var url, body, data;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    url = url_1.buildUrl(BASE_URL, "pushtx?" + CORS_FLAG);
                    if (!(typeof FormData !== 'undefined')) return [3 /*break*/, 1];
                    // Web
                    return [2 /*return*/, new Promise(function (res, rej) {
                            var body = new FormData();
                            body.append('tx', rawTransactionHex);
                            var request = new XMLHttpRequest();
                            request.open('POST', url);
                            request.onload = function () {
                                res();
                            };
                            request.onerror = function () {
                                rej(request.statusText);
                            };
                            request.send(body);
                        })];
                case 1:
                    body = new FormDataFallback();
                    body.append('tx', rawTransactionHex);
                    return [4 /*yield*/, axios_1["default"].post(url, body, {
                            headers: body.getHeaders()
                        })];
                case 2:
                    data = (_a.sent()).data;
                    _a.label = 3;
                case 3: return [2 /*return*/];
            }
        });
    });
}
exports.sendTransaction = sendTransaction;
/**
 * Splits a raw transaction to a list of transactions for every output
 * @param tx The raw transaction
 * @param address The address related to the transaction, to know wich input / output to choose.
 * @param ignoreCashbackOutputs Ignore outputs with same address as input, probably used for cashback
 */
function txToTransactions(tx, address, ignoreCashbackOutputs) {
    var transactions = [];
    var addressRelation = addressRelationToTransaction(tx, address);
    for (var _i = 0, _a = tx.out; _i < _a.length; _i++) {
        var out = _a[_i];
        // If the related address is a sender, set is as the sender, otherwise set first input as we can only specify one sender
        var sender = (addressRelation === 'sender') ? address : tx.inputs[0].prev_out.addr;
        if (ignoreCashbackOutputs) {
            if (addressRelation === 'sender' && wallet_1.isSameAddress(address, out.addr))
                continue;
        }
        // Ignore related input - output combinations
        if (!wallet_1.isSameAddress(sender, address) && !wallet_1.isSameAddress(out.addr, address))
            continue;
        transactions.push({
            hash: tx.hash,
            date: new Date(tx.time * 1000),
            fromAddress: sender,
            toAddress: out.addr,
            amount: new bignumber_js_1.BigNumber(out.value),
            crypto: constants_1.CRYPTO
        });
    }
    return transactions;
}
/**
 * Checks wether an address is rather a sender or a recipient or is unrelated to a transaction
 * by checking if the total amout as input is bigger than the total amount as output.
 * If the amount is the same, consider that the address is a sender.
 * @param tx The raw transaction
 * @param address The related address
 */
function addressRelationToTransaction(tx, address) {
    var amountAsSender = 0;
    var amountAsRecipient = 0;
    for (var _i = 0, _a = tx.inputs; _i < _a.length; _i++) {
        var input = _a[_i];
        if (wallet_1.isSameAddress(input.prev_out.addr, address))
            amountAsSender += input.prev_out.value;
    }
    for (var _b = 0, _c = tx.out; _b < _c.length; _b++) {
        var output = _c[_b];
        if (wallet_1.isSameAddress(output.addr, address))
            amountAsRecipient += output.value;
    }
    if (amountAsSender === 0 && amountAsRecipient === 0)
        return 'unrelated';
    if (amountAsSender > amountAsRecipient)
        return 'sender';
    if (amountAsRecipient > amountAsSender)
        return 'recipient';
    return 'sender';
}

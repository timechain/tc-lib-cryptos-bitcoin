import { BigNumber } from 'bignumber.js';
import { Transaction } from 'tc-lib-cryptos/dist/models';
export interface TxOutput {
    spent: boolean;
    tx_index: number;
    type: number;
    addr: string;
    value: number;
    n: number;
    script: string;
}
export interface Tx {
    ver: string;
    inputs: {
        sequence: string;
        witness: string;
        prev_out: TxOutput;
        script: string;
    }[];
    weight: number;
    block_height: number;
    relayed_by: string;
    out: TxOutput[];
    lock_time: number;
    result: number;
    size: number;
    time: number;
    tx_index: number;
    vin_sz: number;
    hash: string;
    vout_sz: number;
}
export interface BalanceResponse {
    [address: string]: {
        final_balance: number;
        n_tx: number;
        total_received: number;
    };
}
export interface AddressResponse {
    hash160: string;
    address: string;
    n_tx: number;
    total_received: number;
    total_sent: number;
    final_balance: number;
    txs: Tx[];
}
export interface UTXO {
    tx_hash: string;
    tx_hash_big_endian: string;
    tx_index: number;
    tx_output_n: number;
    script: string;
    value: number;
    value_hex: string;
    confirmations: number;
}
export interface UXTOResponse {
    notice: string;
    unspent_outputs: UTXO[];
}
export declare function getBalance(address: string): Promise<BigNumber>;
export declare function getRawTransactions(address: string): Promise<Tx[]>;
export declare function getUnspentTransactions(address: string): Promise<UTXO[]>;
export declare function getTransactions(address: string): Promise<Transaction[]>;
export declare function sendTransaction(rawTransactionHex: string): Promise<void>;
